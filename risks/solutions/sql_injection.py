import sqlite3

def get_user_info(username):
    conn = sqlite3.connect('example.db')
    cursor = conn.cursor()
    query = "SELECT * FROM users WHERE username = ?"
    cursor.execute(query, (username,))
    result = cursor.fetchone()
    conn.close()
    return result

# Example usage
username = "admin'--"
print(get_user_info(username))


# Explanation: The fixed code uses parameterized queries, which help prevent SQL injection by safely inserting user inputs into the query.