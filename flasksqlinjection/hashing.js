const crypto = require('crypto');

function hashPassword(password) {
  // Generate a random salt
    const salt = crypto.randomBytes(16).toString('hex');
  // Hash the combined salt and password
    const hash = crypto.createHmac('sha256', salt)
                    .update(password)
                    .digest('hex');
  // Store the salt and hashed password together
  return `${salt}:${hash}`;
}

function verifyPassword(storedHash, password) {
  // Extract the salt and hashed password from the stored value
  const [salt, originalHash] = storedHash.split(':');
  // Hash the provided password with the extracted salt
  const hash = crypto.createHmac('sha256', salt)
                    .update(password)
                    .digest('hex');
  return hash === originalHash;
}
