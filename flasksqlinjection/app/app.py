from flask import Flask, request, render_template, redirect, url_for, g
import sqlite3

app = Flask(__name__)
DATABASE = 'example.db'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        # Enable foreign key constraints (optional, but recommended for SQLite)
        db.execute("PRAGMA foreign_keys = ON")
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['POST'])
def login():
    try:
        username = request.form['username']
        password = request.form['password']

        db = get_db()
        cursor = db.cursor()

        # Example of vulnerable query (DO NOT USE IN PRODUCTION)
        query = f"SELECT * FROM users WHERE username = '{username}' AND password = '{password}'"
        cursor.execute(query)
        user = cursor.fetchone()

        if user:
            return f"<h1>Welcome, {user[1]}!</h1>"
        else:
            return "<h1>Invalid credentials</h1>"

    except Exception as e:
        return f"An error occurred: {str(e)}"

if __name__ == '__main__':
    # Initialize the database if it doesn't exist
    init_db()
    app.run(host='0.0.0.0', port=5000, debug=True)