-- schema.sql

-- Drop table if exists (for testing purposes)
DROP TABLE IF EXISTS users;

-- Create users table
CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    password TEXT NOT NULL
);

-- Insert some initial data
INSERT INTO users (username, password) VALUES ('admin', 'admin123');

INSERT INTO users (username, password) VALUES ('user1', 'password1');

INSERT INTO users (username, password) VALUES ('user2', 'password2');