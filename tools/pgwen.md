Step-by-Step Guide:

1. Install pwgen

   On Ubuntu/Debian: `sudo apt-get install pwgen`

   On macOS (using Homebrew):`brew install pwgen`

2. Generate a Strong Password:

   To generate a strong password with 16 characters: `pwgen -s 16 1`

   The -s flag ensures the password is secure, 16 is the length of the password, and 1 is the number of passwords to generate.

3. Alternative Tool: openssl `openssl rand -base64 12`

   The -base64 flag encodes the password in base64, and 12 ensures a 16-character output after base64 encoding.

   This command generates a secure password encoded in base64, providing strong randomness.
