# IDGEO - security exercises

## Security risks: analysis

To get better acquainted with the nature of different security risks you will analyze different files and identify
the risk.

Once the risk has been identified you will attempt to mitigate it by applying the right measures.
Do not hesitate to consult external sources to help you with this.

## Secret messages

To get acquainted with the encrypting and decrypting methods we will do a small exercise in creating an RSA key pair and then sending
an encrypted message to a classmate who will be able to decrypt the message with their private key.

## SQL injection

Since you have been able to identify the risk of SQL injection and should have an idea of mitigating it, you will now step into the
shoes of a hacker and play a little game to get access to confidential information.