SQL Injection Exercise
=======================

Objective of the Exercise: Understand SQL Injection.

This application displays a list of fruits. The fruit with the identifier number three is not displayed. We will call it "the forbidden fruit."

The goal is to:

1. Display the name of the "forbidden fruit";
2. Display the password of a user.

All of this is possible through SQL injection.

The debugger should give you useful hints about the fruits and a reading of the code.

To avoid testing all possible combinations, here are the DDL (Data Definition Language - SQL queries that create tables) queries:

```sql
CREATE TABLE public.fruits (
    id bigint,
    label text
);

CREATE TABLE public.users (
    id bigint NOT NULL,
    username text,
    password text
);
```

The `users` table has two users, with identifiers 1 and 2.

Run the Application
--------------------

### Using docker-compose

```
docker-compose up
```

The application should be available at http://127.0.0.1:5000/

A pgadmin4 utility is available at http://127.0.0.1:5001/. Login: `admin@admin.blop`, password: `password`. You then need to configure access to the server:

    host: `db`;
    user: `postgres`
    password: `postgres`
    port: `5432`

### Running Locally

0. Install dependencies: `pip install -r requirements.txt`
1. Start a PostgreSQL database and load the `bdd.sql` file into this database.

This can be done using pgadmin4: create a database, open a query window, and copy-paste the contents of the `bdd.sql` file.

2. Configure the `app.py` file to connect to the database. The connection credentials are located at the beginning of the file (replace with your own values):

```
db = 'foo'
user = 'postgres'
password = 'postgres'
host = '127.0.0.1'
port = '5432'
```

3. Set environment variables:

On linux:

```
export FLASK_APP=app.py
export FLASK_ENV=development
```

On Windows:

```
set FLASK_APP=app.py
set FLASK_ENV=development
```

On Windows / powershell:

```
$env:FLASK_APP = "app.py"
$env:FLASK_ENV = "development"
```

4. Start the application:

```
flask run
```

The application should be available at http://127.0.0.1:5000/


