from flask import Flask
from flask import request
from flask import render_template
import logging
import psycopg2

app = Flask(__name__)

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s:%(levelname)s:%(message)s')
logger = logging.getLogger('app')

db = 'foo'
user = 'postgres'
password = 'postgres'
host = '127.0.0.1'
port = '5432'


@app.route('/')
def hello():
    conn = psycopg2.connect("dbname={} user={} password={} host={} port={}".format(db, user, password, host, port))
    cur = conn.cursor()
    cur.execute("SELECT id, label FROM fruits");
    fruits = cur.fetchall()
    logger.debug("{}".format(fruits))
    cur.close()
    conn.close()
    return render_template("list.html", fruits=fruits)

@app.route('/fruit')
def hello_fruit():
    conn = psycopg2.connect("dbname={} user={} password={} host={} port={}".format(db, user, password, host, port))
    fruit_id = request.args.get('fruit_id', '')
    logger.debug("parameter fruit_id is {}".format(fruit_id));
    cur = conn.cursor()
    query = "SELECT id, label FROM fruits WHERE id = %s"
    logger.debug('Query will be : "{}"'.format(query))
    cur.execute(query, (fruit_id));
    fruit = cur.fetchone()
    logger.debug('Result of the query is {}'.format(fruit))
    cur.close()
    conn.close()
    return render_template("view.html", fruit=fruit)


