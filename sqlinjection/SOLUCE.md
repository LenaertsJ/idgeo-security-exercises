Display another fruit than the one requested by the URL:

`http://localhost:5000/fruit?fruit_id=1;SELECT%20*%20FROM%20fruits%20WHERE%20id%20=%203`

Display the password of the user with id = 1:

`http://localhost:5000/fruit?fruit_id=1;SELECT%20id,%20password%20FROM%20users%20WHERE%20id%20=%201`

(the password is added to the second parameter, displayed by the template)


How SQL Injection Works:

    URL Parameters:
        The web application receives fruit_id as a parameter in the URL, such as http://localhost:5000/fruit?fruit_id=1.

    Vulnerability:
        The application constructs SQL queries using these parameters without proper input validation or sanitization. For example, in the backend code, the fruit_id parameter might directly be used in a SQL query to fetch details about a specific fruit.

    Exploiting SQL Injection:

    Example 1: Display another fruit than the one requested by the URL

    URL: http://localhost:5000/fruit?fruit_id=1;SELECT%20*%20FROM%20fruits%20WHERE%20id%20=%203
        In this URL, after the legitimate fruit_id=1, a semicolon (;) is used to terminate the original SQL query.
        Then, a new SQL query is appended (SELECT * FROM fruits WHERE id = 3).
        This effectively modifies the intended behavior of the SQL query executed by the backend.
        Instead of retrieving the fruit with id=1, the application executes the appended query as well, potentially displaying information about another fruit (in this case, with id=3).

    Example 2: Display the password of the user with id = 1

    URL: http://localhost:5000/fruit?fruit_id=1;SELECT%20id,%20password%20FROM%20users%20WHERE%20id%20=%201
        Similar to the first example, after the legitimate fruit_id=1, a semicolon (;) is used to terminate the original SQL query.
        Then, a new SQL query is appended (SELECT id, password FROM users WHERE id = 1).
        This query instructs the application to fetch the id and password fields from the users table where id = 1.
        If the application displays or uses this information (for example, in a template), the attacker can retrieve sensitive information such as passwords.

Mitigation:

To prevent SQL injection attacks like these, it's crucial to use parameterized queries or prepared statements in your application code. Parameterized queries separate the SQL logic from the data input, ensuring that inputs are treated only as data and not as executable SQL code.

In the context of Python and SQL with Flask, for instance, using SQLAlchemy's ORM or prepared statements with the sqlite3 module helps mitigate SQL injection risks by handling input sanitization and query construction automatically.

Understanding and mitigating SQL injection vulnerabilities is essential for maintaining the security of web applications that interact with databases.