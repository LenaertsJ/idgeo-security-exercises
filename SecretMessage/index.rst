Exercise: Asymmetric Cryptography with OpenSSL
=============================================

Objective
---------

Students will learn how to generate RSA key pairs, encrypt a message using the public key, and decrypt it using the private key.

Requirements
------------

- A computer with OpenSSL installed.
- Basic knowledge of command-line interface (CLI).

Steps
-----

1. **Generate RSA Key Pair:**
   
   - Open a terminal or command prompt.
   - Generate a 2048-bit RSA private key:

   .. code-block:: bash

       openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048

   - Extract the public key from the private key:

   .. code-block:: bash

       openssl rsa -pubout -in private_key.pem -out public_key.pem

2. **Create a Plaintext Message:**

   - Create a text file named ``message.txt`` with the content:

   .. code-block:: text

       Hello, this is a secret message.

3. **Encrypt the Message with the Public Key:**

   - Use the public key to encrypt the message:

   .. code-block:: bash

       openssl rsautl -encrypt -inkey public_key.pem -pubin -in message.txt -out encrypted_message.bin

4. **Decrypt the Message with the Private Key:**

   - Use the private key to decrypt the message:

   .. code-block:: bash

       openssl rsautl -decrypt -inkey private_key.pem -in encrypted_message.bin -out decrypted_message.txt

5. **Verify the Decrypted Message:**

   - Open the ``decrypted_message.txt`` file to ensure it matches the original ``message.txt`` content.

Question for Students
----------------------

Describe the potential risks and consequences if the public key is tampered with or replaced by an attacker. How can this risk be mitigated in a real-world scenario?
